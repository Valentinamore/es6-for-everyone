export function fight(firstFighter, secondFighter) {
  // return winner
    let firstFighterHealth = firstFighter['health'];
    let secondFighterHealth = secondFighter['health'];

    while (true) {
        secondFighterHealth -= getDamage(firstFighter, secondFighter);

        if (secondFighterHealth <= 0) {
            return firstFighter;
        }

        firstFighterHealth -= getDamage(secondFighter, firstFighter);
        if (firstFighterHealth <= 0) {
            return secondFighter;
        }
    }
}

export function getDamage(attacker, enemy) {
  // damage = hit - block
  // return damage
    const damage = getHitPower(attacker) - getBlockPower(enemy);

    return damage;
}

export function getHitPower(fighter) {
  // return hit power
    const { attack } = fighter;
    const criticalHitChance = Math.random() * 1 + 1;
    const power = attack * criticalHitChance;
    return power;
}

export function getBlockPower(fighter) {
  // return block power
    const { defense } = fighter;
    const getBlockPower = Math.random() * 1 + 1;
    const power = defense * getBlockPower;

    return power;
}
