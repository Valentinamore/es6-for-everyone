import { showModal } from './modal';
import {createElement} from "../helpers/domHelper";

export  function showWinnerModal(fighter) {
    const title = 'Winner';
    const { name } = fighter;
    const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
    nameElement.innerText = name;
    bodyElement.append(nameElement);

    showModal({ title, bodyElement });
}