import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source} = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  // show fighter name, attack, defense, health, image

  nameElement.innerText = ' Name: ' + name;
  attackElement.innerText = ' Attack: ' + attack;
  defenseElement.innerText = ' Defense: ' + defense;
  healthElement.innerText = ' Health: ' + health;
  fighterDetails.append(nameElement, attackElement, defenseElement, healthElement, imgElement);

  return fighterDetails;
}
